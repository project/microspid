MICROSPID PASW
--------------
Microspid � un modulo Drupal 8 che implementa il protocollo SPID senza l'ausilio di librerie esterne,
includendo tutto ci� che � strettamente necessario per far funzionare Drupal come Service Provider
SPID e puntando alla facilit� d'uso e manutenzione.
La presente versione del modulo � compatibile con Drupal 9.
Si tenga presente che il modulo � pensato per siti della Pubblica Amministrazione e non � al momento
applicabile a siti privati.

PREMESSE
--------
1) dopo la generazione del certificato fate in luogo sicuro e protetto una copia della cartella cert,
questa cartella non andr� mai cancellata fin quando si intende utilizzare il SP. Se preferite, potete
spostare i file spid-sp.pem/crt in una cartella fuori dal web e inserire nella configurazione il nuovo
percorso assoluto della cartella.
2) conservare il contenuto della tabella del db drupal denominata microspid_tracking. Questa tabella
contiene il tracciamento di ogni sessione di autenticazione. Ogni due giorni una routine cron si occupa
di cancellare le righe incomplete e quelle presenti da pi� di 24 mesi, secondo le regole SPID.
3) Le implementazioni multisito sono possibili, anche se richiedono la configurazione pi� avanti specificata.

INSTALLAZIONE E CONFIGURAZIONE
------------------------------
L'installazione non dovrebbe porre problemi, � sufficiente seguire la procedura standard in questi casi.
Il pacchetto zip o tarball va scaricato da https://drupal.org/project/microspid .
Una volta installato il modulo entrate nella pagina di configurazione (admin->configurazione->microspid)
e cliccate su "crea certificato". Inserite i dati della vostra pubblica amministrazione e procedete alla
creazione del certificato; verrete riportati alla pagina di configurazione, a questo punto potete attivare
l'autenticazione SPID e compilare i campi della configurazione, in particolare ricordatevi di inserire
il codice IPA, la email ufficiale del vostro SP e il rispettivo numero telefonico (con prefisso interna-
zionale). E' anche importante spuntare la voce "mostrare idp Agid di Test", se prima installazione oppure
aggiornamento dei metadati. Terminate queste operazioni salvate la configurazione.
 Potete a questo punto avviare la procedura Agid per la validazione e il deploy agli IdP (trovate le 
istruzioni sul sito https://spid.gov.it ).
Quando riceverete l'avviso Agid che il deploy � stato effettuato, potete, dopo aver caricato la pagina
di accesso utente, testare il funzionamento del tutto.

CONFIGURAZIONE MULTI-SITO
-------------------------
La configurazione multisito prevede un sito principale (indice del servizio 0) e uno o pi� siti secondari
(indice del servizio 1, 2, 3, etc.). Su tutti i siti andr� installato Microspid. Il sito principale andr�
installato e configurato normalmente, come per l'installazione su sito singolo. I siti secondari andranno
installati normalmente e configurati come segue:
a. Attiva autenticazione SPID � disabilitata dal sistema NON cliccare su genera certificato
b. Indice del servizio: valore numerico a partire da 1
c. Livello di autenticazione SPID: 1 o 2 (preferibile)
d. Eventuale path chiave privata: percorso del filesystem locale del server che punta alla posizione del
   certificato del sito principale; per esempio se il sito secondario � in una sottocartella del sito 
   principale (nuova installazione) potr� andar bene:
   ../sites/default/files/private/microspid/cert
   � molto importante che il path sia corretto e che punti ad una posizione in cui effettivamente si trova
   il certificato del sito principale, altrimenti non funzioner� nulla
e. Entity ID del SP cancella quanto presente e inserisci esattamente lo stesso entityID stabilito per il
   sito principale
f. Salva la configurazione
g. Vai in sito principale Configurazione > MicroSpid , clicca su IMPOSTAZIONE MULTI-PORTALE e
   in Impostazioni multi-portale inserisci la stringa relativa al nuovo servizio impostato sul sito 
   secondario
   Ad esempio, supponendo che l'indice del servizio sia 1, che il sottosito si trovi nella cartella subfolder
   e che il nome del servizio del sottosito sia Nuovo Servizio, dovrai scrivere:
   1|https://www.nometuosito.it/subfolder/microspid_acs|subfolder|Nuovo Servizio|
   Se nello spazio di inserimento testo compare gi� una o pi� righe, vuol dire che � stato gi� mappato uno
   o pi� siti secondari, per cui aggiungi i tuoi dati in una nuova riga.
   Per ulteriori precisazioni consulta la descrizione dell'interfaccia di testo.
h. Clicca su Generare i metadata e accetta rigenerazione dei dati
i. Salva la configurazione
l. Ritorna nel sottosito, attiva l'autenticazione SPID e salva la configurazione.


AUTORE E RINGRAZIAMENTI
Il modulo � stato creato da Paolo Bozzo (pagolo DOT bozzo AT gmail DOT com)
Si ringraziano:
- Maurizio Cavalletti
- Nadia Caprotti
- Antonio Todaro
- Umberto Rosini
- Helios Ciancio
- Massimo Berni
- gli sviluppatori del modulo drupal simplesamlphp_auth.
- gli sviluppatori del framework simplesamlphp
- gli sviluppatori della piattaforma OneLogin
- Rob Richards
