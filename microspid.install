<?php

use Drupal\Core\File\FileSystemInterface;
/**
 * @file
 * The install file for the microspid module.
 */

use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Site\Settings;
use Drupal\Core\Url;
use \Drupal\block\entity\Block;


define('MICROSPIDDIR', \Drupal::service('file_system')->realpath('public://microspid'));
define('MICROSPIDDIRPRIV', \Drupal::service('file_system')->realpath('private://microspid'));
define('MICROSPIDMODPATH', \Drupal::service('extension.list.module')->getPath('microspid'));

/**
 * Implements hook_install().
 */
function microspid_install() {

  //user_role_revoke_permissions(AccountInterface::AUTHENTICATED_ROLE, ['change own password']);

  microspid_create_and_copy();
}

/**
 * Create public template directory e copy file.
 */
function microspid_create_and_copy() {
  $newdir = 'public://microspid';
  \Drupal::service('file_system')->prepareDirectory($newdir, FileSystemInterface::MODIFY_PERMISSIONS || FileSystemInterface::CREATE_DIRECTORY);
  $newdir = 'private://microspid';
  \Drupal::service('file_system')->prepareDirectory($newdir, FileSystemInterface::MODIFY_PERMISSIONS || FileSystemInterface::CREATE_DIRECTORY);
  $newdir = 'private://microspid/cert';
  \Drupal::service('file_system')->prepareDirectory($newdir, FileSystemInterface::MODIFY_PERMISSIONS || FileSystemInterface::CREATE_DIRECTORY);
  /*
  $from = MICROSPIDMODPATH . '/cert/.htaccess';
  $to   = 'private://microspid/cert/.htaccess';
  copy($from, drupal_realpath($to));
  */
  // Make sure permissions for dirs are correct. Needed if installed via drush.
  $stat_public = stat(\Drupal::service('file_system')->realpath('public://'));
  $stat_microspid = stat(MICROSPIDDIR);

  if (isset($stat_public['uid'])) {
    if (isset($stat_microspid['uid']) && $stat_public['uid'] != $stat_microspid['uid']) {
      @chown($stat_microspid[0], $stat_public['uid']);
    }
  }
  if (isset($stat_public['gid'])) {
    if (isset($stat_microspid['gid']) && $stat_public['gid'] != $stat_microspid['gid']) {
      @chgrp($stat_microspid[0], $stat_public['gid']);
    }
  }
  if (!file_exists(MICROSPIDDIR . '/metadata.xml')) {
    $from = MICROSPIDMODPATH . '/metadata/templates/metadata.tpl.xml';
    $to = MICROSPIDDIR . '/metadata.xml';
    if (!copy($from, $to)) {
      \Drupal::messenger()->addWarning(t('Unable to move the microspid template to your files folder, please check file permissions and move the directory %from to %to', array('%from' => $from, '%to' => $to)));
    }
  }
  if (!file_exists(MICROSPIDDIRPRIV . '/spid-entities-idps.xml')) {
    $from = 'https://registry.spid.gov.it/metadata/idp/spid-entities-idps.xml';
    $to = MICROSPIDDIRPRIV . '/spid-entities-idps.xml';
    if (!copy($from, $to)) {
      \Drupal::messenger()->addWarning(t('Unable to move the microspid metadata to your files folder, please check file permissions and move the directory %from to %to', array('%from' => $from, '%to' => $to)));
    }
  }
}

function microspid_update_8001() {
  if (file_exists(MICROSPIDDIR . '/metadata.xml')) {
    $md = simplexml_load_file(MICROSPIDDIR . '/metadata.xml');
    $md->addAttribute('xmlns:xmlns:spid', 'https://spid.gov.it/saml-extensions');
    $cp = $md->addChild('ContactPerson','');
    $cp->addAttribute('contactType', 'other');
    $ext = $cp->addChild('Extensions','');
    $nodesx = $ext->addChild('spid:IPACode','xxx','https://spid.gov.it/saml-extensions');
    $nodesx = $ext->addChild('spid:Public','','https://spid.gov.it/saml-extensions');
    $cp->addChild('EmailAddress','xxx');
    $cp->addChild('TelephoneNumber','+390123456789');
    $destname = \Drupal::service('file_system')->realpath('public://microspid/metadata.xml');
    copy($destname, $destname . '.bak'); // make a backup
    $string = $md->asXML();
    $doc = new DOMDocument();
    $doc->preserveWhiteSpace = FALSE;
    $doc->formatOutput = TRUE;
    $doc->loadXML($string);
    $doc->save($destname);
  }
}
function microspid_update_8002() {
  if (!file_exists(MICROSPIDDIRPRIV . '/spid-entities-idps.xml')) {
    $from = 'https://registry.spid.gov.it/metadata/idp/spid-entities-idps.xml';
    $to = MICROSPIDDIRPRIV . '/spid-entities-idps.xml';
    if (!copy($from, $to)) {
      \Drupal::messenger()->addWarning(t('Unable to move the microspid metadata to your files folder, please check file permissions and move the directory %from to %to', array('%from' => $from, '%to' => $to)));
    }
  }
}

/**
 * Implements hook_uninstall().
 *
function microspid_uninstall() {

  // delete login block
  $my_block = Block::load('spidauthstatus');
  if ($my_block) $my_block->delete();
  // Restore the original user registration directive.
}
*/

/**
 * Implements hook_schema().
 */
function microspid_schema() {
  $schema['microspid_tracking'] = array(
    'description' => 'Tabella per tracciamento transazioni.',
    'fields' => array(
      'AuthnReq_ID'  => array(
        'description' => 'AuthnRequest index',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
      ),
      'AuthnRequest'  => array(
        'description' => 'AuthnRequest XML',
        'type' => 'text',
        'not null' => TRUE,
      ),
      'Response'  => array(
        'description' => 'Response XML',
        'type' => 'text',
        'not null' => FALSE,
      ),
      'AuthnReq_IssueInstant'  => array(
        'description' => 'AuthnRequest IssueInstant',
        'type' => 'varchar',
        'length' => 32,
        'not null' => TRUE,
      ),
      'Resp_ID'  => array(
        'description' => 'Response ID',
        'type' => 'varchar',
        'length' => 255,
        'not null' => FALSE,
      ),
      'Resp_IssueInstant'  => array(
        'description' => 'Response IssueInstant',
        'type' => 'varchar',
        'length' => 32,
        'not null' => FALSE,
      ),
      'Resp_Issuer'  => array(
        'description' => 'Response Issuer',
        'type' => 'varchar',
        'length' => 255,
        'not null' => FALSE,
      ),
      'Timestamp' => array(
        'description' => 'timestamp of the event',
        'type' => 'int',
        'not null' => FALSE,
      ),
    ),
    'primary key' => array('AuthnReq_ID'),
  );
  return $schema;
}

/**
 * Implements hook_requirements().
 */
function microspid_requirements($phase) {
  $requirements = [];

  if ($phase == 'runtime') {
    $config = \Drupal::config('microspid.settings');
    if (!$config->get('activate')) {
      $requirements['microspid'] = [
        'severity'    => REQUIREMENT_INFO,
        'title'       => 'microspid',
        'value'       => t('MicroSPiD authentication is NOT activated'),
        'description' => t('It can be activated on the <a href="@config_page">configuration page</a>.', ['@config_page' => Url::fromRoute('microspid.admin_settings')]),
      ];
    }
  }

  if ($phase == 'install') {
		//$fileSystem = \Drupal::service('file_system');
    // Check if the private file stream wrapper is ready to use.
    //if (!$fileSystem->validScheme('private')) {
    if (empty(Settings::get('file_private_path'))) {
			$requirements['microspid'] = [
				'description'	=> t('MicroSPiD installation could not locate the private:// folder and has aborted.'),
				'severity'		=> REQUIREMENT_ERROR,
			];
		}
  }
  return $requirements;
}

