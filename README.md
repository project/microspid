CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

The Microspid module supports the italian authentication protocol SPID, based on SAML.

 * For a full description of the module please read the italian readme
   file (LEGGIMI.md)

 * To submit bug reports and feature suggestions, or to track changes visit:
   https://www.drupal.org/project/issues/microspid


REQUIREMENTS
------------

This module requires the following module:

 * External Authentication module
 
INSTALLATION
------------

Install the Microspid module as you would normally install a Drupal module.


CONFIGURATION
-------------

    Please see the italian readme file (LEGGIMI.md)


MAINTAINERS
-----------

 * Paolo Bozzo (pagolo) - https://www.drupal.org/u/pagolo
 * Maurizio Cavalletti (maurizio_akabit) - https://www.drupal.org/u/maurizio_akabit