<?php

namespace Drupal\microspid\Service;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Site\Settings;

/**
 * Service to interact with the Spid Certs.
 */
class CertsManager {
  protected $config;
  /**
   * {@inheritdoc}
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->config = $config_factory->get('microspid.settings');
  }
  /**
   * @return bool
   *   private key path exsistence
   */
  public function certExists() {
    return file_exists($this->getPrivateKeyPath());
  }
  /**
   * @return string
   *   private key path 
   */
  public function getPrivateKeyPath() {
    $path = $this->config->get('privatepath');
    if (empty($path)) {
      $path = \Drupal::service('file_system')->realpath('private://microspid') . '/cert';
      //return $path . '/spid-sp.pem';
    }
    return $path . '/spid-sp.pem';    
  }

  /**
   * @param string $cert
   *   the certificate
   * @param bool $heads
   *   add headers?
   * @return string
   *   formatted certificate
   */
  public function formatCert($cert, $heads = FALSE) {
    $x509cert = str_replace(array("\x0D", "\r", "\n"), "", $cert);
    if (!empty($x509cert)) {
      $x509cert = str_replace('-----BEGIN CERTIFICATE-----', "", $x509cert);
      $x509cert = str_replace('-----END CERTIFICATE-----', "", $x509cert);
      $x509cert = str_replace(' ', '', $x509cert);

      if ($heads) {
        $x509cert = "-----BEGIN CERTIFICATE-----\n" . chunk_split($x509cert, 64, "\n") . "-----END CERTIFICATE-----\n";
      }

    }
    return $x509cert;
  }

  /**
   * @param string $file
   *   certificate file path
   * @return mixed
   *   formatted certificate | FALSE
   */
  public function getCert($file) {
    $key = file_get_contents($file);
    return $key === FALSE ? FALSE : $this->formatCert($key);
  }
  
  private function OpensslConfig($config, $path) {
    $openssl_config = fopen($path . "/spid-php-openssl.cnf", "w");
      fwrite($openssl_config, "oid_section = spid_oids\n");

      fwrite($openssl_config, "\n[ req ]\n");
      fwrite($openssl_config, "default_bits = 3072\n");
      fwrite($openssl_config, "default_md = sha256\n");
      fwrite($openssl_config, "distinguished_name = dn\n");
      fwrite($openssl_config, "encrypt_key = no\n");
      fwrite($openssl_config, "prompt = no\n");
      fwrite($openssl_config, "req_extensions  = req_ext\n");

      fwrite($openssl_config, "\n[ spid_oids ]\n");
      fwrite($openssl_config, "organizationIdentifier=2.5.4.97\n");
      fwrite($openssl_config, "spid-privatesector-SP=1.3.76.16.4.3.1\n");
      fwrite($openssl_config, "spid-publicsector-SP=1.3.76.16.4.2.1\n");
      fwrite($openssl_config, "uri=2.5.4.83\n");

      fwrite($openssl_config, "\n[ dn ]\n");
      fwrite($openssl_config, "organizationName=" . $config['organizationName'] . "\n");
      fwrite($openssl_config, "commonName=" . $config['commonName'] . "\n");
      fwrite($openssl_config, "uri=" . $config['uri'] . "\n");
      fwrite($openssl_config, "organizationIdentifier=" . $config['organizationIdentifier'] . "\n");
      fwrite($openssl_config, "countryName=" . $config['countryName'] . "\n");
      fwrite($openssl_config, "localityName=" . $config['localityName'] . "\n");
      //fwrite($openssl_config, "serialNumber=" . $config['spOrganizationCode'] . "\n");

      fwrite($openssl_config, "\n[ req_ext ]\n");
      fwrite($openssl_config, "certificatePolicies = @spid_policies\n");

      fwrite($openssl_config, "\n[ spid_policies ]\n");
      fwrite($openssl_config, "policyIdentifier = spid-publicsector-SP\n");
      fclose($openssl_config);
      /*
      switch ($config['spIsPublicAdministration']) {
          case 'yes': fwrite($openssl_config, "policyIdentifier = spid-publicsector-SP\n");
              break;
          case 'no': fwrite($openssl_config, "policyIdentifier = spid-privatesector-SP\n");
              break;

          default:
              echo $colors->getColoredString("Your Organization type is not correctly set. Please retry installation. Found: " . $config['spIsPublicAdministration'] . "\n", "red");
              fwrite($openssl_config, "ERROR- Interrupted\n");
              fclose($openssl_config);
              die();
              break;
      */
    
  }

  /**
   * @param array $dn
   *   params for certificate
   * 
   */
  public function makeCerts($dn) {
    $numberofdays = 3652 * 2;
    $privkey = openssl_pkey_new(array(
      'private_key_bits' => 2048,
      'private_key_type' => OPENSSL_KEYTYPE_RSA,
      'x509_extensions' => 'v3_ca',
      'digest_alg' => 'sha256',
    ));
    $path = $this->config->get('privatepath');
    if (empty($path)) {
      $path = \Drupal::service('file_system')->realpath('private://microspid') . '/cert';
    }
    $this->OpensslConfig($dn, $path);
    $config_args = array(
      'config' => $path . "/spid-php-openssl.cnf",
      'req_extensions' => 'req_ext',
    );
    $csr = openssl_csr_new($dn, $privkey, $config_args);
    $myserial = hexdec(uniqid());

    // Do cert.
    $configArgs = array("digest_alg" => "sha256");
    $sscert = openssl_csr_sign($csr, NULL, $privkey, $numberofdays, $configArgs, (int) $myserial);
    openssl_x509_export($sscert, $publickey);
    openssl_pkey_export($privkey, $privatekey);

    $pathname = $path . '/spid-sp';
    file_put_contents($pathname . '.pem', $privatekey);
    file_put_contents($pathname . '.crt', $publickey);
    unlink($path . "/spid-php-openssl.cnf");
  }
}
